export const Constants = {
    navLinks: [
        {
            label: 'User',
            path: './user',
            index: 0
        }, {
            label: 'Todo App',
            path: './todo',
            index: 1
        }
    ]
};
