﻿export const environment = {
  production: true,
  endpoint: {
    user: 'http://localhost:3001/auth',
    todo: 'http://localhost:3001/api/todo'
  }
};