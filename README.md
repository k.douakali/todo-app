﻿## Getting Started

In the root directory of the project run ["docker-compose up -d"] to launch the app

## File Structure

The back-end is based on Express js at ./server.
The front-end is based on [Angular cli "ng"] ./client.
The database is based on Mongo DB with the official docker image.

The front-end is served on http://localhost:3000/ and the back-end on http://localhost:3001/.

This project was created using [Microsoft Web Template Studio](https://github.com/Microsoft/WebTemplateStudio).
